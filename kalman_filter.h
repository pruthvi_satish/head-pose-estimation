#ifndef KALMAN_FILTER_H_
#define KALMAN_FILTER_H_
// #include "Dense"
#include <Eigen/Eigen>
#include <Eigen/Dense>

using Eigen::MatrixXd;
using Eigen::VectorXd;

class KalmanFilter {
public:


	VectorXd x_; //* state vector

	MatrixXd P_; //* state covariance matrix

	MatrixXd F_; //* state transistion matrix

	MatrixXd Q_; //* process covariance matrix

	MatrixXd H_; //* measurement matrix

	MatrixXd R_; //* measurement covariance matrix

	KalmanFilter(); // Constructor

	virtual ~KalmanFilter(); // Destructor

	/**
	 * Predict Predicts the state and the state covariance
	 * using the process model
	 */
	void Predict();

	/**
	 * Updates the state and
	 * @param z The measurement at k+1
	 */
	void Update(const VectorXd &z);

};

#endif /* KALMAN_FILTER_H_ */

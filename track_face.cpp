#include "track_face.h"

#define CAFFE

DetectTrackFace::DetectTrackFace() {

  #ifdef TF
    net = cv::dnn::readNetFromTensorflow(tensorflowWeightFile, tensorflowConfigFile);
  #else
    net = cv::dnn::readNetFromCaffe(caffeConfigFile, caffeWeightFile);
  #endif

  tracker_init = false;
  ok = false;
  tracker = TrackerKCF::create();

  count = 0;
  async_locked = false;

}

DetectTrackFace::~DetectTrackFace() {

}

// wrapper to check if the async process is ready or not
template<typename R>
bool is_ready(std::future<R> const& f, int time_){
  return f.wait_for(std::chrono::seconds(time_)) == std::future_status::ready;
}

void DetectTrackFace::detectFaceOpenCVDNN(Net net, Mat &frameOpenCVDNN, Rect2d &bbox)
  {
    int frameHeight = frameOpenCVDNN.rows;
    int frameWidth = frameOpenCVDNN.cols;

    #ifdef TF
        cv::Mat inputBlob = cv::dnn::blobFromImage(frameOpenCVDNN, inScaleFactor, cv::Size(frameWidth, frameHeight), meanVal, true, false);
    #else
        cv::Mat inputBlob = cv::dnn::blobFromImage(frameOpenCVDNN, inScaleFactor, cv::Size(frameWidth, frameHeight), meanVal, false, false);
    #endif

    net.setInput(inputBlob, "data");
    cv::Mat detection = net.forward("detection_out");

    cv::Mat detectionMat(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());

    for(int i = 0; i < detectionMat.rows; i++)
    {
        float confidence = detectionMat.at<float>(i, 2);
        if(confidence > confidenceThreshold)
        {
            int x1 = static_cast<int>(detectionMat.at<float>(i, 3) * frameWidth);
            int y1 = static_cast<int>(detectionMat.at<float>(i, 4) * frameHeight);
            int x2 = static_cast<int>(detectionMat.at<float>(i, 5) * frameWidth);
            int y2 = static_cast<int>(detectionMat.at<float>(i, 6) * frameHeight);

            bbox.x = double(x1);
            bbox.y = double(y1);
            bbox.width = double(x2 - x1);
            bbox.height = double(y2 - y1);
        }
    }

  }

void DetectTrackFace::KCF_tracker(Ptr<Tracker> &tracker, Mat &frame, Rect2d &bbox, bool &ok){
    // Update the tracking result
    ok = tracker->update(frame, bbox);
    if (ok)
    {
        // Tracking success : Draw the tracked object
        cout << "Tracking Success!!" << endl;
    }
    else
    {
        cout << "Tracking Falied!!" << endl;
    }
}

void DetectTrackFace::Make_Square(Rect2d &bbox){

  // before making sqare let's create an offset to extend the height
  int offset_y = int(bbox.height * 0.1);
  bbox.y += offset_y;
  bbox.height += offset_y;


  int diff;
  diff = bbox.height - bbox.width;
  if(diff == 0){
    return;
  }

  int left_x = bbox.x;
  int top_y = bbox.y;
  int right_x = bbox.x + bbox.width;
  int bottom_y = bbox.y + bbox.height;

  int delta = int(abs(diff) / 2);

  if (diff > 0){
    left_x -= delta;
    right_x += delta;

    if(diff % 2 == 1){
      right_x += 1;
    }
    bbox.x = left_x;
    bbox.width = right_x - left_x;
  }
  else{
    top_y -= delta;
    bottom_y += delta;

    if(diff % 2 == 1){
      bottom_y += 1;
    }
    bbox.y = bottom_y;
    bbox.height = bottom_y - top_y;
  }

}

void DetectTrackFace::ProcessDetections(Mat &frame, Rect2d &bbox){

    double t = cv::getTickCount();

    if (bbox.width != 0 && tracker_init == false){
        tracker->init(frame, bbox);
        tracker_init = true;
    }

    if (count % detection_refresh_rate == 0 && (!async_locked)){
        cout << " REINITIALIZING DETECTION!!! " << endl;

        // sparse_async_detection = std::async(detectFaceOpenCVDNN, net,frame, bbox);
        sparse_async_detection = std::async(std::launch::async, [&] { return DetectTrackFace::detectFaceOpenCVDNN(net, frame, bbox); });

        async_locked = true;
        count = 0;
    }

    count += 1;

    if(async_locked){
      if (is_ready(sparse_async_detection, 0)) {
        sparse_async_detection.get();
        if (bbox.width != 0){
          tracker->clear();
          tracker = TrackerKCF::create();
          tracker->init(frame, bbox);
          ok = true;
        }
        async_locked = false;
      }
    }

    if (ok){
        DetectTrackFace::KCF_tracker(tracker, frame, bbox, ok);
    }
    else if(async_locked){ // lost tracking
        if (is_ready(sparse_async_detection, 0)) {
          sparse_async_detection.get();
          if (bbox.width != 0){
            tracker->clear();
            tracker = TrackerKCF::create();
            tracker->init(frame, bbox);
            ok = true;
          }
          async_locked = false;
        }
    }
    else{ // might be a bit jittery here
        DetectTrackFace::detectFaceOpenCVDNN (net, frame, bbox);
        if (bbox.width != 0)
          tracker->clear();
          tracker = TrackerKCF::create();
          tracker->init(frame, bbox);
          ok = true;
  }

  DetectTrackFace::Make_Square(bbox);

}

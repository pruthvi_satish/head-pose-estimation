#ifndef STABILIZE_KF_H_
#define STABILIZE_KF_H_

#include "measurement_package.h"
#include <vector>
#include <string>
#include <fstream>
#include "kalman_filter.h"

class Stabilize {
public:
	Stabilize();
	virtual ~Stabilize();
	void ProcessMeasurement(const MeasurementPackage &measurement_pack);
	KalmanFilter kf_;
  VectorXd predicted_;
  VectorXd updated_;

private:
	bool is_initialized_;
	int64_t previous_timestamp_;

	//acceleration noise components
	float noise_ax;
	float noise_ay;


};

#endif /* STABILIZE_KF_H_ */

#ifndef MEASUREMENT_PACKAGE_H_
#define MEASUREMENT_PACKAGE_H_

// #include "Dense"
#include <Eigen/Eigen>
#include <Eigen/Dense>

class MeasurementPackage {
public:
	int64_t timestamp_;
	Eigen::VectorXd raw_measurements_;
};

#endif /* MEASUREMENT_PACKAGE_H_ */

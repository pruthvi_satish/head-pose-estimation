import tensorflow as tf
from tensorflow.python.platform import gfile

def load_graph(frozen_graph_filename):
    # We load the protobuf file from the disk and parse it to retrieve the
    # unserialized graph_def
    with tf.gfile.GFile(frozen_graph_filename, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())

    # Then, we can use again a convenient built-in function to import a graph_def into the
    # current default Graph
    with tf.Graph().as_default() as graph:
        tf.import_graph_def(
            graph_def,
            input_map=None,
            return_elements=None,
            name='',#name="prefix",
            op_dict=None,
            producer_op_list=None
        )
    return graph


# We use our "load_graph" function
graph = load_graph('/home/ryuzaki/Desktop/test_frozen_model/frozen_graph.pb')

# We can verify that we can access the list of operations in the graph
for op in graph.get_operations():
    print(op.name)     # <--- printing the operations snapshot below

# We access the input and output nodes
x = graph.get_tensor_by_name('image_tensor:0')
y = graph.get_tensor_by_name('layer6/final_dense:0')


import numpy as np
import cv2
CNN_INPUT_SIZE = 128
face_img = cv2.imread('square_face.jpg')
face_img = cv2.resize(face_img, (CNN_INPUT_SIZE, CNN_INPUT_SIZE))
face_img = cv2.cvtColor(face_img, cv2.COLOR_BGR2RGB)

image_np = [face_img]

# We launch a Session
with tf.Session(graph=graph) as sess:
    # compute the predicted output for test_x
    predictions = sess.run( y, feed_dict={x: image_np} )
    marks = np.array(predictions).flatten()[:136]

    print(marks)

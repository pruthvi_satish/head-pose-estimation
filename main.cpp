#include "pose_visualizer.h"
#include "stabilize.h"
#include "track_face.h"

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <Eigen/Eigen>
#include <Eigen/Dense>
#include <chrono>

using namespace std;
using namespace cv;
using Eigen::VectorXd;
using namespace std::chrono;

Point pt;

static void mouseHandler( int event, int x, int y, int, void* )
{
   if ( event == EVENT_MOUSEMOVE )
   {
        pt.x = x;
        pt.y = y;

   }
}

void test_kf_stabilizer(){
  // testing Kalman Filter's ability to track mouse pointer

  namedWindow("Mouse Tracking");
  Mat frame(cv::Size(640, 480), CV_8UC3, Scalar(0)); // black
  // CV_8UC3 or CV_64FC1 ?

  // static void mouseHandler(int event,int x,int y, int flags,void* param);
  setMouseCallback("Mouse Tracking", mouseHandler, &frame); //mouse callback function;

  int64_t time_;

  Stabilize s;

  Point predicted_state;
  Point updated_state;

  while(1){

    time_ = duration_cast< milliseconds >(system_clock::now().time_since_epoch()).count();
    MeasurementPackage meas_package;

    meas_package.raw_measurements_ = VectorXd(2);
    meas_package.raw_measurements_ << pt.x, pt.y;
    meas_package.timestamp_ = time_;

    s.ProcessMeasurement(meas_package);

    predicted_state.x = s.predicted_(0);
    predicted_state.y = s.predicted_(1);
    updated_state.x = s.updated_(0);
    updated_state.y = s.updated_(1);

    // cout << " predicted_state : " << predicted_state.x << " " << predicted_state.y << endl;

    // circle(frame, pt, 2, Scalar(0, 0, 255), 1, 8); // ground-truth mouse location
    circle(frame, predicted_state, 2, Scalar(0, 255, 0), 1, 8); // ground-truth mouse location
    circle(frame, updated_state, 2, Scalar(255, 0, 0), 1, 8); // ground-truth mouse location
    imshow( "Mouse Tracking", frame);

    char key = cvWaitKey(10);
    if (key == 27) // ESC
      break;
  }

}

void test_pose_visualizer(){
  Pose_Visualize viz;

  // test draw
  Mat rvec = (Mat_<double>(3,1) << 0.05993085, 0.3694817, -3.0130892);
  Mat tvec = (Mat_<double>(3,1) << 259.21695, 101.78604, -933.1361);
  viz.Draw_axis(rvec, tvec);
}


void test_face_tracking(){

  int video_source = 0;
  VideoCapture cap(video_source);

  if (!cap.isOpened())
  {
      cout << "!!! Failed to open file: " << video_source << std::endl;
  }

  Mat frame;
  Rect2d bbox;

  double tt_opencvDNN = 0;
  double fpsOpencvDNN = 0;

  DetectTrackFace dtf;

  while(1){

    if (!cap.read(frame))
        break;

    double t = cv::getTickCount();

    // here the magic happens!
    dtf.ProcessDetections(frame, bbox);

    cv::rectangle(frame, bbox, cv::Scalar(0, 255, 0), 2, 4);

    tt_opencvDNN = ((double)cv::getTickCount() - t)/cv::getTickFrequency();
    fpsOpencvDNN = 1/tt_opencvDNN;
    putText(frame, format("OpenCV DNN ; FPS = %.2f",fpsOpencvDNN), Point(10, 50), FONT_HERSHEY_SIMPLEX, 1.4, Scalar(0, 0, 255), 4);
    imshow( "OpenCV - DNN Face Detection",frame);

    char key = cvWaitKey(10);
    if (key == 27) // ESC
        break;

  }

}

int main(){

  // test_pose_visualizer();

  //test_kf_stabilizer();

  test_face_tracking();

  return 0;
}

// run using (until I make a build script):
// g++ -I /usr/local/include/eigen3/ `pkg-config --cflags opencv` main.cpp pose_visualizer.cpp `pkg-config --libs opencv`

//update :
// g++ -I /usr/local/include/eigen3/ `pkg-config --cflags opencv` main.cpp stabilize.cpp kalman_filter.cpp pose_visualizer.cpp `pkg-config --libs opencv`

// update - 2:
// g++ -I /usr/local/include/eigen3/ `pkg-config --cflags opencv` main.cpp stabilize.cpp kalman_filter.cpp pose_visualizer.cpp `pkg-config --libs opencv` -lpthread

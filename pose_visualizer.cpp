#include "pose_visualizer.h"

Pose_Visualize::Pose_Visualize() {
}

Pose_Visualize::~Pose_Visualize() {
}

void Pose_Visualize::Draw_axis(Mat &rvec, Mat &tvec) {
  points.push_back(Point3d(30,0,0));
  points.push_back(Point3d(0,30,0));
  points.push_back(Point3d(0,0,30));
  points.push_back(Point3d(0,0,0));
  projectPoints(points, rvec, tvec, camera_matrix, dist_coeefs, projectedPoints);

  // for(int i = 0; i < projectedPoints.size(); i++){
  //   cout << projectedPoints[i] << endl;
  // }

  // add more code here
}

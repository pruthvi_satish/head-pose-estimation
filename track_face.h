#ifndef TRACK_FACE_H_
#define TRACK_FACE_H_

#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/core/ocl.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <future>
#include <chrono>
#include <stdlib.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/dnn.hpp>

using namespace cv;
using namespace std;
using namespace cv::dnn;

class DetectTrackFace{
  public:
    DetectTrackFace();
    virtual ~DetectTrackFace();

    void ProcessDetections(Mat &frame, Rect2d &bbox);

    const string caffeConfigFile = "./worker_assets/deploy.prototxt";
    const string caffeWeightFile = "./worker_assets/res10_300x300_ssd_iter_140000_fp16.caffemodel";
    const string tensorflowConfigFile = "./worker_assets/opencv_face_detector.pbtxt";
    const string tensorflowWeightFile = "./worker_assets/opencv_face_detector_uint8.pb";


  private:
    void detectFaceOpenCVDNN(Net net, Mat &frameOpenCVDNN, Rect2d &bbox);
    void Make_Square(Rect2d &bbox);

    void KCF_tracker(Ptr<Tracker> &tracker, Mat &frame, Rect2d &bbox, bool &ok);

    const size_t detection_refresh_rate = 4;
    const double inScaleFactor = 1.0;
    const float confidenceThreshold = 0.7;
    const cv::Scalar meanVal = cv::Scalar(104.0, 177.0, 123.0); // might wanna check on this later
    Net net;

    bool tracker_init;
    bool ok;
    Ptr<Tracker> tracker;

    int count;
    bool async_locked;
    std::future<void> sparse_async_detection;

};


#endif /* TRACK_FACE_H_ */

#ifndef POSE_VISUALIZER_H_
#define POSE_VISUALIZER_H_

#include <iostream>
#include <vector>
#include <Eigen/Eigen>
#include <Eigen/Dense>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>

using namespace std;
using namespace cv;

class Pose_Visualize {
public:

  Pose_Visualize();

  virtual ~Pose_Visualize();

  Mat camera_matrix = (Mat_<double>(3,3) <<
    640, 0, 320,
    0, 640, 240,
    0, 0, 1);

  Mat dist_coeefs = (Mat_<double>(4,1) << 0 , 0 , 0, 0);

  vector<Point3f> points;
  vector<Point2f> projectedPoints;

  void Draw_axis(Mat &rvec, Mat &tvec);
};

#endif /* POSE_VISUALIZER_H_ */
